# This file contains quick phony targets to build IPFS packages

ARCH ?= amd64
OS ?= linux

ifeq ($(ARCH),arm)
	DEB_ARCH := armhf
endif
DEB_ARCH ?= $(ARCH)

IPFS_VERSION?=$(shell curl --silent "https://api.github.com/repos/ipfs/go-ipfs/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
IPFS_PKG_VERSION:=$(shell echo ${IPFS_VERSION} | sed 's/v//')-0

_download_ipfs:
	mkdir -p build/orig/
	curl -L --output build/orig/ipfs-${IPFS_VERSION}-${ARCH}.tar.gz \
		https://github.com/ipfs/go-ipfs/releases/download/${IPFS_VERSION}/go-ipfs_${IPFS_VERSION}_${OS}-${ARCH}.tar.gz
.PHONY: _download_ipfs

_unpack_ipfs: _download_ipfs
	rm -rf build/source/go-ipfs/${ARCH}
	mkdir -p build/source/go-ipfs/${ARCH}
	tar -xvzf build/orig/ipfs-${IPFS_VERSION}-${ARCH}.tar.gz -C build/source/go-ipfs/${ARCH}
.PHONY: _unpack_ipfs

_prepare_ipfs_deb: _unpack_ipfs
	mkdir -p build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}
	cp -aR source/ipfs.deb/* build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/
	chmod +x build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/debian/rules
	# Updates details of the package
	sed -i 's/$${VERSION}/${IPFS_PKG_VERSION}/' build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/debian/control
	sed -i 's/$${DEB_ARCH}/${DEB_ARCH}/' build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/debian/control
	# Updates the pseudo-changelog
	sed -i 's/$${VERSION}/${IPFS_PKG_VERSION}/' build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/debian/changelog
	sed -i 's/$${DATE}/$(shell date -R)/' build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/debian/changelog
	# Move the source here
	cp -aR build/source/go-ipfs/${ARCH}/go-ipfs build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION}/
.PHONY: _prepare_ipfs_deb

_build_ipfs_deb_package: _prepare_ipfs_deb
	cd build/deb/${DEB_ARCH}/ipfs-${IPFS_PKG_VERSION} ;\
	dpkg-buildpackage -us -uc -d --host-arch ${DEB_ARCH}
.PHONY: ipfs_deb_package

deb_ipfs_%:
	$(MAKE) clean_build \
		_build_ipfs_deb_package \
		collect_deb_packages \
		-e ARCH=$*
