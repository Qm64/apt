# This file contains quick phony targets to build IPFS packages

GTW_PKG_VERSION:=1.0.0-0

EMAIL ?= "hello@qm64.tech"
DEBFULLNAME ?= "Qm64 OU"

_prepare_ipfs-gateway_deb:
	mkdir -p build/deb/all/ipfs-gateway-${GTW_PKG_VERSION}
	cp -aR source/ipfs-gateway.deb/* build/deb/all/ipfs-gateway-${GTW_PKG_VERSION}/
	# Updates details of the package
	sed -i 's/$${VERSION}/${GTW_PKG_VERSION}/' build/deb/all/ipfs-gateway-${GTW_PKG_VERSION}/debian/control
	# Updates the pseudo-changelog
	sed -i 's/$${VERSION}/${GTW_PKG_VERSION}/' build/deb/all/ipfs-gateway-${GTW_PKG_VERSION}/debian/changelog
	sed -i 's/$${DATE}/$(shell date -R)/' build/deb/all/ipfs-gateway-${GTW_PKG_VERSION}/debian/changelog
	chmod +x build/deb/all/ipfs-gateway-${GTW_PKG_VERSION}/debian/rules

.PHONY: _prepare_ipfs-gateway_deb

_build_ipfs-gateway_deb_package: _prepare_ipfs-gateway_deb
	cd build/deb/all/ipfs-gateway-${GTW_PKG_VERSION} ;\
	dpkg-buildpackage -us -uc -d
.PHONY: ipfs_deb_package

deb_ipfs-gateway:
	$(MAKE) clean_build \
		_prepare_ipfs-gateway_deb _build_ipfs-gateway_deb_package \
		collect_deb_packages