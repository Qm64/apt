# Welcome to Qm64 Repository

This is the place where all the packages and distributions built or
maintained by Qm64 lives. This includes a GNU/Linux Repository as
well as basic downloadable binaries for Windows and Mac for various
products.

Note: All the packages are automatically built monthly or on changes.

Please [contact us](https://qm64.tech/) if you have questions!

## Add the Debian repository on Ubuntu

To add Qm64 Debian Repository you can run the following commands in your
terminal:

```shell
wget -qO - https://apt.qm64.tech/key.public.asc | sudo apt-key add -
echo "deb https://apt.qm64.tech/ apt/" | sudo tee -a /etc/apt/sources.list.d/Qm64.list
sudo apt update
```
